import os
from PIL import Image

def resize_image(event, context):
    image_path = event.get('image_path')
    output_path = event.get('output_path')
    width = event.get('width')
    height = event.get('height')

    try:
        image = Image.open(image_path)
        resized_image = image.resize((width, height))
        resized_image.save(output_path)
        return {"message": "Image resized successfully"}
    except Exception as e:
        return {"error": str(e)}

# Дополнительные функции обработки изображений могут быть добавлены здесь

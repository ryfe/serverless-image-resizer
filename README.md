# Serverless Image Resizer

This is a serverless application that resizes images using AWS Lambda and Python.

## Functionality

The application provides an HTTP endpoint to resize images. When a POST request is made to the `/resize` endpoint with the image path, output path, width, and height parameters, the application resizes the image and saves it to the specified output path.

## Requirements

- Python 3.8
- Serverless Framework
- serverless-python-requirements plugin
- Pillow library

## Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/your/repository.git
   

2. Install dependencies:

   ```bash
   cd serverless-image-resizer
   npm install

3. Deploy the application:
   ```bash
   sls deploy


## Usage

Send a POST request to the `/resize` endpoint with the following parameters:
- image_path: path to the input image file
- output_path: path to save the resized image
- width: desired width of the resized image
- height: desired height of the resized image

Example:
```bash
curl -X POST https://your-api-endpoint/resize -d "image_path=input.jpg&output_path=output.jpg&width=300&height=200"

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.